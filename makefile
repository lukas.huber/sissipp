flags = -Wall -Wextra -std=c++17

link: compile
	g++ $(flags) -o sissi2 main.o -lboost_program_options -lbpp-core -lbpp-phyl -lbpp-seq
	rm *.o

compile:
	g++ -c $(flags) main.cpp
