// Simulating Site Specific Interactions_SISSI
// Copyright (C) 2019 Lukas Huber, Tanja Gesell

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "data_collection.hpp"
#include "file_reader/nei_reader.cpp"
#include "file_reader/fasta_reader.cpp"
#include "config.hpp"
#include <iostream>

void DATA_COLLECTION::initNeighbourhood() {
    using namespace std;

    string path = vm["neighbourhood"].as<string>();
    string file_format = path.substr(path.find_last_of('.') + 1);

    try  {
        /* Check if file format is matching to .nei, then parse  it */
        if (file_format == "nei") {
            NEI_READER nei_reader;
            nei_reader.readNEIFile(path);
            neighbourhood = nei_reader.getNeighbourhood();
            if (neighbourhood.getSize() == 0 || neighbourhood.getParsedSites() == 0){
                throw std::logic_error("There was no neighbourhood to parse");
            }
        }
        else {
            cout << "File format of neighbourhood file is not supported." << endl;

            if (!DEBUG) {
                cout << "Aborting program" << endl;
                exit(EXIT_FAILURE);
            }
        }
    } catch (exception& e) {
        cerr << e.what() << endl;
        exit(1);
    } catch (...) {
        cerr << "Undefined errors occured while neighbourhood input" << endl;
        exit(1);
    }
    
    if (DEBUG){
        cout << "##### NEIGHBOURHOOD DEBUG OUTPUT #####" << endl
             << "  Fileformat to read:\t\t" << path.substr(path.find_last_of('.') + 1) << endl
             << "  Parsed neighbourhood length:\t" << neighbourhood.getParsedSites() << endl
             << "  Total connections found:\t" << neighbourhood.getSize() << endl
             << "  pos 1 connected to:\t" << neighbourhood.getValue(1) << endl
             << "  pos 2 connected to:\t" << neighbourhood.getValue(2) << endl
             << "  pos 3 connected to:\t" << neighbourhood.getValue(3) << endl
             << "  pos 4 connected to:\t" << neighbourhood.getValue(4) << endl << endl;
    }
}

void DATA_COLLECTION::initTree() {
    using namespace std;

    string path = vm["tree"].as<string>();
    string file_format = path.substr(path.find_last_of('.') + 1);

    try {
        /* Check if file format is matching to .tree or .dnd, then parse it */
        if (file_format == "tree" || file_format == "dnd") {
            bpp::Newick *newick_reader = new bpp::Newick(true);  // Allows comments in tree file
            tree = newick_reader->read(path);                    // Tree in file
        }
        else {
            cout << "File format of tree file is not supported." << endl;

            if (!DEBUG) {
                cout << "Aborting program" << endl;
                exit(EXIT_FAILURE);
            }
        }
    } catch (bpp::Exception & e) {
        cerr << "ERROR! " << e.what() << std::endl;
        exit(1);
    } catch (exception& e) {
        cerr << e.what() << endl;
        exit(1);
    } catch (...) {
        cerr << "Undefined errors occured during tree input" << endl;
        exit(1);
    }

    if (DEBUG){
        cout << "##### TREE DEBUG OUTPUT ##############" << endl
             << "  Fileformat to read:\t\t" << file_format << endl
             << "  Number of Nodes found:\t" << tree->getNumberOfNodes() << endl
             << "  Numbere of leaves found:\t" << tree->getNumberOfLeaves() << endl << endl;
    }
}

void DATA_COLLECTION::initRNG() {
    setRNGMethod();     // Save RNG method chosen by user

    /* Set seed if one is provided by user, else generate one */
    if (getSeedChoice())
        rng->setSeed(vm["seed"].as<uint64_t>());
    else
        rng->generateSeed();

    if (rng->getSeed() != 0){
        if (rng_method == "JKISS") {
            rng = new JKISS_64(rng->getSeed());
        } 
        else if (rng_method == "MT19937") {
            rng = new MT19937(rng->getSeed());
        }
    } 
    else {
        if (rng_method == "JKISS") {
            rng = new JKISS_64();
        } 
        else if (rng_method == "MT19937") {
            rng = new MT19937();
        }
    }

    /* Print DEBUG output and test if only unique numbers are generated */
    if (DEBUG) {
        vector<double>cont = {};
        double temp = 0;
        bool duplicates = false;

        /* Fill cont with random numbers */
        for (int i = 0; i < 1000000; i++) {
            temp = rng->drawNumber();
            cont.push_back(temp);
        }

        /* Check cont for duplicates */
        std::sort(cont.begin(), cont.end());
        for(long unsigned int i = 0; i < cont.size() - 1; i++) {
            if (cont[i] == cont[i + 1]) {
                cont.erase(cont.begin() + i);
                i--;
                duplicates = true;
            }
        }

        cout << "##### RNG DEBUG OUTPUT ###############" << endl
             << "  RNG method:\t\t\t" << rng_method << endl
             << "  RNG seeding value:\t\t" << rng->getSeed() << endl
             << "  RNG duplicates present:\t" << duplicates << endl << endl;
             
    }
}

void DATA_COLLECTION::initSequence() {
    using namespace std;

    /* Parse sequence from file if given by user, else create random ancestor sequence */
    if (vm.count("sequence")){
        FASTA_READER fasta_reader;                                  // Initialize file reader
        string sequencefile_path = vm["sequence"].as<string>();     // Get file path from argument for later use

        fasta_reader.parseFastaFile(sequencefile_path, sites, alphabet);      // Initialize sequence (read and parse file) and store in "variable sites"
    }
    else {
        model.normalizeFrequencies();                           // Making sure, that sum of frequencies adds up to 1
        neighbourhood.initNk();
        ancestral_sequence = createRandomAncestralSequence();
    }

    if (DEBUG){
        string sequence_provided = "False";

        if (vm.count("sequence")) {
            sequence_provided = "True";
        }

        cout << "##### SEQUENCE DEBUG OUTPUT ##########" << endl
             << "  Sequence was provided:\t" << sequence_provided << endl 
             << "  Parsed sequences:\t\t" << sites->getNumberOfSequences() << endl
             << "  Parsed sites:\t\t\t" << sites->getNumberOfSites() << endl;

        if (!vm.count("sequence")) {
            cout << "  Ancestral Sequence:\t\t" << "Successfully generated" << endl << endl;
        }
        else {
            cout << endl;
        }
    }
}

void DATA_COLLECTION::initAlphabet() {      // TODO: Exchange switch and magic numbers with better code.
    using namespace std;

    int alphabet_choice;
    string alpha;

    /* Set alphabet and save it. Default = RNA */
    if (vm.count("DNA") && vm["DNA"].as<bool>() == true)
        alphabet_choice = 1;
    else if (vm.count("RNA") && vm["RNA"].as<bool>() == true)
        alphabet_choice = 2;
    else
        throw std::runtime_error("Errors occoured while setting alphabet choice");

    switch (alphabet_choice) {
        case 1:
            alphabet = new bpp::DNA();
            alpha = "DNA";
            break;
        case 2:
            alphabet = new bpp::RNA();
            alpha = "RNA";
            break;
        default:
            throw logic_error("Error during initialization of alphabet.");
    }

    sites = new bpp::VectorSiteContainer(alphabet);

    if (DEBUG) {
        cout << "##### ALPHABET DEBUG OUTPUT ##########" << endl
             << "  Alphabet used:\t\t" << alpha << endl << endl;
    }
}

void DATA_COLLECTION::initFrequencies() {
    using namespace std;

    /* Parse single frequencies from command line */
    if (vm.count("single")) {
        model.setSingleFrequencies(getFrequenciesFromArguments("single", 4));
    }

    if (vm.count("double")) {
        model.setDoubleFrequencies(getFrequenciesFromArguments("double", 16));
    }

    if (vm.count("triple")) {
        model.setTripleFrequencies(getFrequenciesFromArguments("triple", 64));
    }

    if (DEBUG) {
        cout << "##### FREQUENCIES DEBUG OUTPUT #######" << endl;
        model.printSingleFrequencies();
        model.printDoubleFrequencies();
        model.printTripleFrequencies();
    }
}

void DATA_COLLECTION::initParameters() {
    /* No parameters are initialized for model "JC69" and "F81" */

    if (substitution_model == "K80" || substitution_model == "HKY85" || substitution_model == "T92") {
        bool kappa1_set = !vm["kappa1"].empty();
        bool kappa2_set = !vm["kappa2"].empty();

        if (kappa1_set && kappa2_set) {
            double kappa1 = vm["kappa1"].as<double>();
            double kappa2 = vm["kappa2"].as<double>();

            model.setKappa(kappa1 + kappa2);
        } 
        else if (kappa1_set && !kappa2_set) {
            double kappa1 = vm["kappa1"].as<double>();

            model.setKappa(kappa1);
        }
        else if (!kappa1_set && kappa2_set) {
            double kappa2 = vm["kappa2"].as<double>();

            model.setKappa(kappa2);
        }
        else {
            throw logic_error("Error during parameter initialization. Need parameter 'kappa1' and/or 'kappa2'.");
        }
    }
    else if (substitution_model == "TN93") {
        bool kappa1_set = !vm["kappa1"].empty();
        bool kappa2_set = !vm["kappa2"].empty();

        if (kappa1_set && kappa2_set) {
            double kappa1 = vm["kappa1"].as<double>();
            double kappa2 = vm["kappa2"].as<double>();

            model.setKappa1(kappa1);
            model.setKappa2(kappa2);
        }
        else {
            throw logic_error("Error during parameter initialization. Need parameter 'kappa1' and 'kappa2'.");
        }
    }
    else if (substitution_model == "GTR") {
        bool GTRa_set = !vm["GTRa"].empty();
        bool GTRb_set = !vm["GTRb"].empty();
        bool GTRc_set = !vm["GTRc"].empty();
        bool GTRd_set = !vm["GTRd"].empty();
        bool GTRe_set = !vm["GTRe"].empty();
        bool GTRf_set = !vm["GTRf"].empty();
        
        if (GTRa_set && GTRb_set && GTRc_set &&
            GTRd_set && GTRe_set && GTRf_set) {
                double GTRa = vm["GTRa"].as<double>();
                double GTRb = vm["GTRb"].as<double>();
                double GTRc = vm["GTRc"].as<double>();
                double GTRd = vm["GTRd"].as<double>();
                double GTRe = vm["GTRe"].as<double>();
                double GTRf = vm["GTRf"].as<double>();

                model.setGTRa(GTRa / GTRf);
                model.setGTRb(GTRb / GTRf);
                model.setGTRc(GTRc / GTRf);
                model.setGTRd(GTRd / GTRf);
                model.setGTRe(GTRe / GTRf);
                model.setGTRf(GTRf);
        }
        else {
            throw logic_error("Error during parameter initialization. Need parameter GTRa, GTRb, GTRc, GTRd, GTRe and GTRf.");
        }
    }

    if (DEBUG) {
        cout << "##### MODEL PARAMETERS OUTPUT #####" << endl
             << "  Model name:\t" << model.getName() << endl
             << "  Kappa1:\t" << model.getKappa1() << endl
             << "  Kappa2:\t" << model.getKappa2() << endl
             << "  GTRa:\t\t" << model.getGTRa() << endl
             << "  GTRb:\t\t" << model.getGTRb() << endl
             << "  GTRc:\t\t" << model.getGTRc() << endl
             << "  GTRd:\t\t" << model.getGTRd() << endl
             << "  GTRe:\t\t" << model.getGTRe() << endl
             << "  GTRf:\t\t" << model.getGTRf() << endl << endl;
    }
}

bpp::Sequence *DATA_COLLECTION::createRandomAncestralSequence() {
    std::vector<int> sequence;
    sequence.resize(neighbourhood.getParsedSites(), -1);    // Resize the vector to number of parsed sites from neighbourhood file. Fill with illegal ints.

    double tmp = 0;
    int counter = 0;
    double randomnumber = 0;

    int tmp_index = 0;
    int first_letter = 0;
    int second_letter = 0;
    int third_letter = 0;

    /* Fill sequence with positions according to their Nk drawn from discrete distribution */
    for (unsigned int pos = 0; pos < neighbourhood.getParsedSites(); pos++){
        randomnumber = rng->drawNumber();
        tmp = 0;
        counter = 0;

        if (sequence[pos] == -1 ) {
            if (neighbourhood.getNk(pos) == 0) {
                do {
                    tmp += model.getSingleFrequencies()[counter];
                    tmp_index = counter++;
                } while(randomnumber > tmp);
                sequence[pos] = tmp_index;

            } else if (neighbourhood.getNk(pos) == 1) {
                do {
                    tmp += model.getDoubleFrequencies()[counter];
                    tmp_index = counter++;
                } while(randomnumber > tmp);

                first_letter =  (((tmp_index+4)/4) -1)%4;
                second_letter = tmp_index - (4*first_letter);
                sequence[pos] = first_letter;
                sequence[neighbourhood.getValue(pos)] = second_letter;

            } else if (neighbourhood.getNk(pos) == 2) {
                do {
                    tmp += model.getTripleFrequencies()[counter];
                    tmp_index = counter++;
                } while(randomnumber > tmp);

                first_letter =  (((tmp_index+16)/16) -1)%4;
                second_letter =  (((tmp_index+4)/4) -1)%4;
                third_letter = tmp_index - ( (16*first_letter) + (4*second_letter) );
                sequence[pos] = first_letter;
                sequence[neighbourhood.getValue(pos)] = second_letter;
                sequence[neighbourhood.getValue(pos), 2] = third_letter;
            } else {
                throw std::overflow_error("Ancestral sequence can not be processed with more than 2 neighbours (Nk=2).");
            }
        }
    }

    return new bpp::BasicSequence("random_ancestral_sequence", sequence, alphabet);
}