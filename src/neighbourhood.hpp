// Simulating Site Specific Interactions_SISSI
// Copyright (C) 2019  Lukas Huber, Tanja Gesell

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SISSIPP_NEIGHBOURHOOD_HPP
#define SISSIPP_NEIGHBOURHOOD_HPP

#include <unordered_map>    // Used structural system
#include <vector>           // Used to save number of connection per site

using namespace std;

class NEIGHBOURHOOD {
private:
    unsigned int parsed_sites;                         // Number of parsed sites in neighbourhood file            
    unordered_multimap<int, int> structural_system;    // Map to save connections    
    vector<int> nk;                                    // Number of connections per site

public:
    /* Constructors */
    NEIGHBOURHOOD() {
     parsed_sites = 0;
        structural_system.clear();
        nk.clear();
    }

    /* Destructors */
    virtual ~NEIGHBOURHOOD() = default;

    /* Prototypes */
    void addNeighbour(int, int);        // Used to add neighbours to map
    void initNk();                      // Used to store the number of connection (Nk) for each site of the sequence
    bool existElement(int) const;       // Used to check wheter a specific position is present

    /* GETTERs */
    unsigned int getParsedSites() const {
        return parsed_sites;
    }

    unsigned long getSize() const {
        return structural_system.size();
    }

    int getValue(const int key, int appearance = 1) const {
        auto match = structural_system.equal_range(key);
        for (auto i = match.first; i != match.second; i++) {
            for (auto j = 1; j < appearance; j++) {
                i++;
            }
            return i->second;
        }
        return 0; // To handle warning during compiling
    }

    const vector<int> &getNk() const {
        return nk;
    }

    const int &getNk(int i) const {
        return nk[i];
    }

    /* SETTERS */
    void setParsedSites(unsigned int parsed_sites) {
        NEIGHBOURHOOD::parsed_sites = parsed_sites;
    }
};

#endif