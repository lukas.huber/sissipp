// Simulating Site Specific Interactions_SISSI
// Copyright (C) 2019  Lukas Huber, Tanja Gesell

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "neighbourhood.hpp"
#include <iostream>

void NEIGHBOURHOOD::addNeighbour(int neigh1, int neigh2){
    structural_system.insert( {neigh1, neigh2} );
    structural_system.insert( {neigh2, neigh1} );
}

void NEIGHBOURHOOD::initNk() {
    using namespace std;

    nk.resize(parsed_sites, 0);     // Resize the vector to match the parsed_sites and fill it with zeros

    for (unsigned int i = 0; i < parsed_sites; i++) {
        auto range = structural_system.equal_range(i);
        for (auto j = range.first; j != range.second; ++j) {    // Add one to nk, if the element exists
            nk[i]++;
        }
    }
}

bool NEIGHBOURHOOD::existElement(int position) const {
        return structural_system.count(position);
}