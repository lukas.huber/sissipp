// Simulating Site Specific Interactions_SISSI
// Copyright (C) 2019 Lukas Huber, Tanja Gesell

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "rng.hpp"

/* External initialization of static member */
uint64_t RNG::seed = 0;

/* Throw an error, since no number can be drawn from RNG directly, but must
   be drawn from one of its subclasses (MT19937 or JKISS) */
double RNG::drawNumber() {      
    throw std::logic_error("RNG engine not yet defined!");
}

void RNG::generateSeed() {
    /* Generate UUID */
    boost::uuids::random_generator generator;
    boost::uuids::uuid uuid = generator();

    /* Generate HASH of UUID */
    boost::hash<boost::uuids::uuid> uuid_hasher;
    uint64_t uuid_hash = uuid_hasher(uuid);

    /* Get thread ID */
    std::thread::id thread_id = std::this_thread::get_id();

    /* Generate HASH of thread ID */
    std::hash<std::thread::id> thread_hasher;
    uint64_t thread_id_hash = thread_hasher(thread_id);

    /* Get current time */
    long time = std::chrono::high_resolution_clock::now().time_since_epoch().count();

    /* Concatenate UUID, thread ID and time and save to seed */
    seed = uuid_hash + time + thread_id_hash;
};