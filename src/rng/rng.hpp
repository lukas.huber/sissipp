// Simulating Site Specific Interactions_SISSI
// Copyright (C) 2019 Lukas Huber, Tanja Gesell

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SISSIPP_RNG_HPP
#define SISSIPP_RNG_HPP

#include <iostream>                         // Used for visualization on terminal
#include <chrono>                           // Used to get time component for seed
#include <thread>                           // Used to get thread ID component for seed
#include <boost/uuid/uuid.hpp>              // Used to get UUID component for seed
#include <boost/uuid/uuid_generators.hpp>   // Necessary to get UUID       
#include <boost/functional/hash.hpp>        // Used to hash each used component

class RNG {
private:
    static uint64_t seed;

public:
    /* Variables */
    static const uint64_t upper_limit = std::numeric_limits<uint64_t >::max();
    static const long lower_limit = std::numeric_limits<uint64_t >::min();

    /* Constructors */
    RNG () {
        generateSeed();
    }

    /* Destructors */
    virtual ~RNG() = default;

    /* Operator overloading */
    virtual double operator() () {
        return drawNumber();
    }

    /* Prototypes */ 
    virtual double drawNumber();    // Returns a random number (must be overloaded by one of it subclasses)
    void generateSeed();            // Generate a seed based on UUID, thread ID and time */

    /* Print seed to cmd */
    void printSeedValue() {
        std::cout << "RNG Seeding value :" << seed << std::endl;
    }
    
    /* SETTER */
    void setSeed (uint64_t value) {
        seed = value;
    }

    /* GETTER */
    uint64_t getSeed() const {
        return seed;
    }
};

#endif