// Simulating Site Specific Interactions_SISSI
// Copyright (C) 2019 Lukas Huber, Tanja Gesell

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SISSIPP_MT19337_64_HPP
#define SISSIPP_MT19337_64_HPP

#include "rng.hpp"
#include <random>

class MT19937 : public RNG {
private:
    std::mt19937_64 engine;

public:
    /* Constructors */
    MT19937() {
        engine.seed(RNG::getSeed()); 
    }

    MT19937(unsigned long seed) {
        RNG::setSeed(seed);
        MT19937::setSeed(RNG::getSeed());
    };

    /* Destructors */
    virtual ~MT19937() = default;

    /* Operator overloading */
    double operator() () {
        return engine();
    }

    /* SETTERs */
    void setSeed(unsigned long seed) {
        engine.seed(seed);
    }

    /* GETTERs */
    double drawNumber() {
        return static_cast<double>(engine()) / upper_limit;
    }
};

#endif