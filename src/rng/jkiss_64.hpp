// Simulating Site Specific Interactions_SISSI
// Copyright (C) 2019 Lukas Huber, Tanja Gesell

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SISSIPP_JKISS_64_HPP
#define SISSIPP_JKISS_64_HPP

#include "rng.hpp"
#include <iostream>

class JKISS_64 : public RNG {
private:
    uint64_t v, x, y, z;                            // Seed variables. This generator needs more than 1 seed variable.
    uint64_t tmp_1, tmp_2;                          // Temp variables
    uint64_t state;                                 // State variable
    static const long mod = 5187321857949412318;    // Random number used for bit shifting

public:
    /* Constructors */
    JKISS_64() {
        JKISS_64::setSeed(RNG::getSeed());
    }

    JKISS_64(unsigned long seed_value) {
        RNG::setSeed(seed_value);
        JKISS_64::setSeed(RNG::getSeed());
    };

    /* Destructors */
    virtual ~JKISS_64() = default;

    /* Functions */
    void setSeed(unsigned long seed) {
        tmp_1 = std::abs(mod - abs(long(seed)));
        tmp_1 %= upper_limit;
        tmp_2 = seed;

        // Random numbers
        v = 224897412;
        x = 127891846;
        y = 187423387;
        z = 108790423;
    }

    double drawNumber() {
        /* CNG Generator */
        tmp_1 = 1490024343005336237ULL * tmp_1 + 123456789;

        /* XORSHIFT Generator */
        tmp_2 ^= tmp_2 << 21;
        tmp_2 ^= tmp_2 >> 17;
        tmp_2 ^= tmp_2 << 30; /* Do not set y=0! */

        /* B64MWC Generator */
        state = 4294584393ULL * v + x; x = state >> 32; v = state;
        state = 4246477509ULL * y + z; z = state >> 32; y = state;

        double randomnumber = static_cast<double>(tmp_1 + tmp_2 + v + (y << 32)) / upper_limit;

        return randomnumber;
    }
};

#endif