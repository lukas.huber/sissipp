// Simulating Site Specific Interactions_SISSI
// Copyright (C) 2019 Lukas Huber, Tanja Gesell

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>                     // Used for console output
#include "argument_parser.hpp"          // Helper file to parse arguments
#include <iomanip>                      // Used for std::setprecision()
#include "config.hpp"                   // Used for VERSION

void parseArguments(int argc, char** argv, ARGUMENTS& argue_container) {
    using std::string, std::cout, std::endl, std::vector;

    /* File variables. Used to store filepaths */
    string nfile;   // Neighbourhood file
    string tfile;   // Tree file
    string sfile;   // Sequence file

    /* Vectors to store frequencies */
    vector<double> singlefreqs, doublefreqs, triplefreqs;

    /* RNG variables. Used to store info, which rng is used and seed */
    bool JKISS, MT19937;    // RNG methods
    unsigned long seed;     // Seed

    /* Model variables. Used to store which substitution model should be used */
    bool JC69, K80, F81, HKY85, T92, TN93, GTR;                     // Nucleotide models
    bool DS078, JTT92, WAG01;                                       // Protein models
    bool GY94, MG94, YN98;                                          // Codon models
    double kappa1, kappa2, GTRa, GTRb, GTRc, GTRd, GTRe, GTRf;      // Model parameters

    /* Alphabet variable. Used to store which alphabet is used in the sequence */
    bool RNA, DNA;   // Alphabets

    try {
        /* Generate group of basic options */
        po::options_description generic("Generic options");
        generic.add_options()
                ("version,v",   "Print version message")
                ("help,h",      "Print help message")
                ("verbose",     "Print verbose output");

        /* Generate group of required options */
        po::options_description required("Required options");
        required.add_options()
                ("neighbourhood,n", po::value(&nfile)->value_name("path"), "Sets path to neighbourhood file")
                ("tree,t",          po::value(&tfile)->value_name("path"), "Sets path to tree file")
                ("single", po::value(&singlefreqs)->value_name("frequencies")->multitoken(),
                        "4 'single' nucleotide frequencies")
                ("double", po::value(&doublefreqs)->value_name("frequencies")->multitoken(),
                        "16 'doublet' nucleotide frequencies")
                ("triple", po::value(&triplefreqs)->value_name("frequencies")->multitoken(),
                        "64 'triplet' nucleotide frequencies");
        
        /* Generate group of required options */
        po::options_description sequence("Sequence options");
        sequence.add_options()
                ("sequence,s",      po::value(&sfile)->value_name("path"), "Sets path to sequence file");

        po::options_description alphabet("Alphabet options");
        alphabet.add_options()
                ("DNA",     po::bool_switch(&DNA)->default_value(false),    "Sets alphabet of sequence to DNA")
                ("RNA",     po::bool_switch(&RNA)->default_value(true),     "Sets alphabet of sequence to RNA");

        /* Generate group of RNG options */
        po::options_description rng("Random seed generator");
        rng.add_options()
                ("JKISS,j",     po::bool_switch(&JKISS)->default_value(false),      "JKISS 64bit (Marsaglia and Zamann, 2011)")
                ("MT19937,m",   po::bool_switch(&MT19937)->default_value(true),     "Mersenne Twister 64bit (Matsumoto and Nishimura, 2000) = DEFAULT")
                ("seed",      po::value<uint64_t>(&seed)->value_name("value"),      "Custom 64bit long seed value \n"
                                                                                    "DEFAULT = system generated (UUID based)");

        /* Generate group of verbose options */
        po::options_description verbose("Verbose options");
        verbose.add_options()
                ("printseed", "Print random generator seed");

        /* Generate group of nucleotide substitution models */
        po::options_description nucleotidemodels("Nucleotide substitution model options");
        nucleotidemodels.add_options()
                ("JC69",    po::value(&JC69)->zero_tokens()->default_value(true),   "Jukes & Cantor model (1969) \n"
                                                                                    "= DEFAULT")
                ("K80",     po::value(&K80)->zero_tokens(),     "Kimura (1980)")
                ("F81",     po::value(&F81)->zero_tokens(),     "Felsenstein (1981)")
                ("HKY85",   po::value(&HKY85)->zero_tokens(),   "Hasegawa, Kishino & Yano (1985)")
                ("T92",     po::value(&T92)->zero_tokens(),     "Tamura (1992)")
                ("TN93",    po::value(&TN93)->zero_tokens(),    "Tamura & Nei (1993)")
                ("GTR",     po::value(&GTR)->zero_tokens(),     "General Time-Reversible substitution model");

        /* Generate group of protein substitution models */
        po::options_description proteinmodels("Protein substitution model options");
        proteinmodels.add_options()
                ("DS078", po::value(&DS078)->zero_tokens(), "Dayhoff, Schwartz & Orcutt")
                ("JTT92", po::value(&JTT92)->zero_tokens(), "Jones, Taylor & Thornton (1992)")
                ("WAG01", po::value(&WAG01)->zero_tokens(), "Whelan & Goldman (2001)");

        /* Generate group of codon substitution models */
        po::options_description codonmodels("Codon substitution model options");
        codonmodels.add_options()
                ("GY94", po::value(&GY94)->zero_tokens(), "Goldman & Yang (1994)")
                ("MG94", po::value(&MG94)->zero_tokens(), "Muse & Gaut (1994)")
                ("YN98", po::value(&YN98)->zero_tokens(), "Yang & Nielsen (1998)");

        /* Generate group of model parameters */
        po::options_description modelparameters("Substitution model parameters");
        modelparameters.add_options()
                ("kappa1", po::value(&kappa1)->value_name("value"), "parameter kappa1")
                ("kappa2", po::value(&kappa2)->value_name("value"), "parameter kappa2")
                ("GTRa",  po::value(&GTRa)->value_name("value"),    "parameter a for GTR model")
                ("GTRb",  po::value(&GTRb)->value_name("value"),    "parameter b for GTR model")
                ("GTRc",  po::value(&GTRc)->value_name("value"),    "parameter c for GTR model")
                ("GTRd",  po::value(&GTRd)->value_name("value"),    "parameter d for GTR model")
                ("GTRe",  po::value(&GTRe)->value_name("value"),    "parameter e for GTR model")
                ("GTRf",  po::value(&GTRf)->value_name("value"),    "parameter f for GTR model")
                ;

        /* Join command line groups together */
        po::options_description cmdline_options;
        cmdline_options.add(required).add(sequence).add(alphabet).add(nucleotidemodels).add(proteinmodels).add(codonmodels).add(modelparameters).add(rng).add(verbose).add(generic);

        /* Parse given options from command line and store it in po::variables_map which ist set as class member of ARGUMENTS */
        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, cmdline_options), argue_container.storeVM());  // Parse Command line arguments

        /* Print help to cmd line */
        if (argue_container.getVm().count("help")) {
            cout << "Simulating Sequence Evolution with Site Specifc Interactions" << endl
                 << "Copyright (C) Lukas Huber, Tanja Gesell" << endl
                 << "lukas.huber@protonmail.com" << endl << endl;

            cout << "For further information see README and LICENSE" << endl << endl;

            cout << "sissi <options> -n <neighbourhood file> -t <tree file>" << endl;

            cout << cmdline_options << endl;
            std::exit(EXIT_SUCCESS);
        }

        /* Print version to cmd */
        if (argue_container.getVm().count("version")) {
            cout << "SISSI version: " << std::fixed << std::setprecision(1) << VERSION << endl;
            std::exit(EXIT_SUCCESS);
        }

    } catch(std::exception& e) {
        std::cerr << e.what() << endl;
        std::exit(1);
    }

}

void ARGUMENTS::checkRequired() {
    bool abort = false;     // Flag used to track if program should be aborted
    if (!vm.count("neighbourhood")){ 
        cout << "No neighbourhood file was provided!" << endl;
        abort = true;
    }

    if (!vm.count("tree")){ 
        cout << "No tree file was provided!" << endl;
        abort = true;
    }

    if (!vm.count("single") && !vm.count("double") && !vm.count("triple")){
        cout << "No frequencies were provided!" << endl;
        abort = true;
    }

    if (abort && !DEBUG) {                      // I don't want the program to abort, if DEBUG is enabled
        cout << "Aborting program." << endl;
        std::exit(EXIT_FAILURE);
    }
}