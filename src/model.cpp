// Simulating Site Specific Interactions_SISSI
// Copyright (C) 2019 Lukas Huber, Tanja Gesell

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "model.hpp"
#include <iostream>

void MODEL::normalizeFrequencies() {

    /* NORMALIZATION IS CURRENTLY BUGGED! FREQUENCIES ARE ONLY CHECKED FOR THEIR SUM! FREQUENCIES MUST ADD UP TO 1! */

    // using namespace std;

    // const int singlefreqs_size = singlefreqs.size();
    // const int doublefreqs_size = doublefreqs.size();
    // const int triplefreqs_size = triplefreqs.size();

    // /* Check if size of all three vectors are correct. Else abort the program. */
    // if (singlefreqs_size != 4)
    //     throw runtime_error("Size of vector of single Frequencies (singlefreqs) not does not equal 4! Aborting program!");
    
    // if (doublefreqs_size != 16)
    //     throw runtime_error("Size of vector of double Frequencies (doublefreqs) not does not equal 16! Aborting program!");
    
    // if (triplefreqs_size != 64)
    //     throw runtime_error("Size of vector of triple Frequencies (triplefreqs) not does not equal 64! Aborting program!");

    // /* Normalize all three frequency-vectors by dividing each position by the vectors size. */
    // for (int i = 0; i < singlefreqs_size; i++) { // Single frequencies vector
    //     if (singlefreqs[i] != 0) { // Avoid division by 0
    //         singlefreqs[i] = singlefreqs[i] / singlefreqs_size;
    //     }
    //     else {
    //         singlefreqs[i] = 0;
    //         cerr << "Position " << i << " of the single frequencies vector is set to 0. This position was hold on 0 during normalization to avoid division by 0." << endl;
    //     }
    // }

    // for (int i = 0; i < doublefreqs_size; i++) { // Double frequencies vector
    //     if (doublefreqs[i] != 0) { // Avoid division by 0
    //         doublefreqs[i] = doublefreqs[i] / doublefreqs_size;
    //     }
    //     else {
    //         doublefreqs[i] = 0;
    //         cerr << "Position " << i << " of the double frequencies vector is set to 0. This position was hold on 0 during normalization to avoid division by 0." << endl;
    //     }
    // }

    // for (int i = 0; i < triplefreqs_size; i++) { // Triple frequencies vector
    //     if (triplefreqs[i] != 0) { // Avoid division by 0
    //         triplefreqs[i] = triplefreqs[i] / triplefreqs_size;
    //     }
    //     else {
    //         triplefreqs[i] = 0;
    //         cerr << "Position " << i << " of the triple frequencies vector is set to 0. This position was hold on 0 during normalization to avoid division by 0." << endl;
    //     }
    // }

    /* Check if every frequency-vector adds up to 1 */
    double sum_singlefreqs = 0;
    double sum_doublefreqs = 0;
    double sum_triplefreqs = 0;

    for (auto& temp : singlefreqs)
        sum_singlefreqs += temp;

    for (auto& temp : doublefreqs)
        sum_doublefreqs += temp;

    for (auto& temp : triplefreqs)
        sum_triplefreqs += temp;

    if (sum_singlefreqs != 1)
        throw logic_error("Single frequencies do not add up to one after normalization!");
    
    if (sum_doublefreqs != 1)
        throw logic_error("Double frequencies do not add up to one after normalization!");
    
    if (sum_triplefreqs != 1)
        throw logic_error("Triple frequencies do not add up to one after normalization!");
}

void MODEL::printSingleFrequencies() {
    using namespace std;

    cout << "  Single frequencies:" << endl;

    for (auto i = singlefreqs.begin(); i != singlefreqs.end(); i++) {
        cout << "\t" << *i << endl;
    }

    cout << endl;
}

void MODEL::printDoubleFrequencies() {
    using namespace std;

    int counter = 0; // Used for linebreaks

    cout << "  Double frequencies:" << endl
         << "\t";

    for (auto i = doublefreqs.begin(); i != doublefreqs.end(); i++) {
        cout << *i << ", ";
        counter++;

        if (counter % 4 == 0) {
            cout << endl << "\t";
        }
    }

    cout << endl;
}

void MODEL::printTripleFrequencies() {
    using namespace std;

    int counter = 0; // Used for linebreaks

    cout << "  Triple frequencies:" << endl
         << "\t";

    for (auto i = triplefreqs.begin(); i != triplefreqs.end(); i++) {
        cout << *i << ", ";
        counter++;

        if (counter % 8 == 0) {
            cout << endl << "\t";
        }
    }

    cout << endl;
}