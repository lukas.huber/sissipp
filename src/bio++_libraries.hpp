// Simulating Site Specific Interactions_SISSI
// Copyright (C) 2019 Lukas Huber, Tanja Gesell

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SISSIPP_BIOPP_LIBRARIES_HPP
#define SISSIPP_BIOPP_LIBRARIES_HPP

/* From libbpp-core-dev */
#include <Bpp/Exceptions.h>         // Used to handle exceptions

/* From libbpp-seq-dev */
#include <Bpp/Seq/Io/Fasta.h>                           // Used for parsing sequence file in fasta format
#include <Bpp/Seq/Container/VectorSequenceContainer.h>  // Used for parsing sequence file in fasta format
#include <Bpp/Seq/Alphabet/DNA.h>                       // Used for parsing sequence file to provide container in specific alphabet (DNA)         
#include <Bpp/Seq/Alphabet/RNA.h>                       // Used for parsing sequence file to provide container in specific alphabet (RNA)   

/* From libbpp-phyl-dev */
#include <Bpp/Phyl/Tree.h>          // Used for the container for tree information
#include <Bpp/Phyl/Io/Newick.h>     // Used for parsing tree file

#endif