// Simulating Site Specific Interactions_SISSI
// Copyright (C) 2019 Lukas Huber, Tanja Gesell

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SISSIPP_MODEL_HPP
#define SISSIPP_MODEL_HPP

#include <iostream>     // Used for visualization on command line
#include <vector>       // Used to store frequecies

using namespace std;

class MODEL {
private:
    string name;                                            // Used to store the name of the chosen model
    vector<double> singlefreqs, doublefreqs, triplefreqs;   // Used to store the given frequencies from arguments

    /* Variables to store model parameters */
    double kappa1, kappa2, theta;
    double GTRa, GTRb, GTRc, GTRd, GTRe, GTRf;

public:
    /* Constructors */
    MODEL() {
        /* By default uniform distributed */
        singlefreqs= {0.25,
                      0.25,
                      0.25, 
                      0.25};

        doublefreqs = {0.0625, 0.0625, 0.0625, 0.0625,
                       0.0625, 0.0625, 0.0625, 0.0625,
                       0.0625, 0.0625, 0.0625, 0.0625,
                       0.0625, 0.0625, 0.0625, 0.0625};

        triplefreqs = {0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625,
                       0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625,
                       0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625,
                       0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625,
                       0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625,
                       0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625,
                       0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625,
                       0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625, 0.015625};
        }
    /* Destructors */
    virtual ~MODEL() = default;

    /* Prototypes */
    void normalizeFrequencies();    // BUGGED FUNCTION, DO NOT USE! NEEDS TO BE REVISED!
    void printSingleFrequencies();
    void printDoubleFrequencies();
    void printTripleFrequencies();

    /* GETTERS */
    vector<double> getSingleFrequencies() {
        return singlefreqs;
    }

    vector<double> getDoubleFrequencies() {
        return doublefreqs;
    }

    vector<double> getTripleFrequencies() {
        return triplefreqs;
    }

    double getKappa() {
        return kappa1 + kappa2;
    }

    double getKappa1() {
        return kappa1;
    }

    double getKappa2() {
        return kappa2;
    }

    string getName() {
        return name;
    }

    double getGTRa() {
        return GTRa;
    }

    double getGTRb() {
        return GTRb;
    }

    double getGTRc() {
        return GTRc;
    }

    double getGTRd() {
        return GTRd;
    }

    double getGTRe() {
        return GTRe;
    }

    double getGTRf() {
        return GTRf;
    }

    /* SETTERS */
    void setSingleFrequencies(vector<double> input) {
        if (input.size() != 4) {
            throw runtime_error("Error during setSingleFrequencies in class MODEL. Size of input vector != 4.");
        }

        singlefreqs.clear(); // Clear vector before it is filled with input vector.

        for (auto i = input.begin(); i != input.end(); i++) {
            singlefreqs.push_back(*i);
        }
    }

    void setDoubleFrequencies(vector<double> input) {
        if (input.size() != 16) {
            throw runtime_error("Error during setDoubleFrequencies in class MODEL. Size of input vector != 16.");
        }   

        doublefreqs.clear(); // Clear vector before it is filled with input vector.

        for (auto i = input.begin(); i != input.end(); i++) {
            doublefreqs.push_back(*i);
        }     
    }

    void setTripleFrequencies(vector<double> input) {
        if (input.size() != 64) {
            throw runtime_error("Error during setTripleFrequencies in class MODEL. Size of input vector != 64.");
        }

        triplefreqs.clear(); // Clear vector before it is filled with input vector.

        for (auto i = input.begin(); i != input.end(); i++) {
            triplefreqs.push_back(*i);
        }   
    }

    /* ASK TANJA ABOUT THIS */
    void setKappa(double input) {
        kappa1 = input / 2;
        kappa2 = input / 2;
    }

    void setKappa1(double input) {
        kappa1 = input;
    }

    void setKappa2(double input) {
        kappa2 = input;
    }

    void setName(string input) {
        name = input;
    }

    void setGTRa(double input) {
        GTRa = input;
    }

    void setGTRb(double input) {
        GTRb = input;
    }

    void setGTRc(double input) {
        GTRc = input;
    }

    void setGTRd(double input) {
        GTRd = input;
    }

    void setGTRe(double input) {
        GTRe = input;
    }

    void setGTRf(double input) {
        GTRf = input;
    }

};

#endif