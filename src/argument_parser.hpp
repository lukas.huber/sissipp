// Simulating Site Specific Interactions_SISSI
// Copyright (C) 2019 Lukas Huber, Tanja Gesell

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SISSIPP_ARGUMENTS_HPP
#define SISSIPP_ARGUMENTS_HPP

#include <iostream>                     // Used for visualization on terminal
#include "boost/program_options.hpp"    // Used for argument parser
#include <vector>                       // Used to store parsed agruments

namespace po = boost::program_options;
using namespace std;

class ARGUMENTS {
private:
    po::variables_map vm;           // Variable map containing all given arguments
    std::string rng_method;         // Variable to store the chosen rng method
    std::string substitution_model; // Variable to store chosen substitution model

public:
    /* Constructors */
    ARGUMENTS() = default;

    /* Destructors */
    virtual ~ARGUMENTS() = default;
    
    po::variables_map &storeVM() {
        return vm;
    }

    const po::variables_map &getVm() const {
        return vm;
    }

    /* Functions */
    void checkRequired();

    /* GETTER */


    const string &getSubModel() const {
        return substitution_model;
    }

    const string &getString(const string &keyvalue) const {
        if (vm.count(keyvalue)) {
            return vm[keyvalue].as<string>();
        } else {
            throw logic_error("Argument doesn't exist: " + keyvalue);
        }
    }

    /* SETTER */

};

void parse_arguments(int , char** , ARGUMENTS&);    // Reads arguments from cmd and saves them in ARGUMENTS container

#endif