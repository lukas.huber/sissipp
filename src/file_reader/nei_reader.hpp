// Simulating Site Specific Interactions_SISSI
// Copyright (C) 2019 Lukas Huber, Tanja Gesell

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SISSIPP_NEI_READER_HPP
#define SISSIPP_NEI_READER_HPP

#include <iostream>             // Used for visualisation on command line
#include <fstream>              // Used for file input

#include "../neighbourhood.cpp" // Used to store neighbourhood information

class NEI_READER {
private:
    unsigned int counter = 0;   // Used to count amount of lines
    std::string line;           // Used as temp variable to store lines during parsing

    unsigned int position;          // 
    int connecting_position;        //
    float number;                   //

    NEIGHBOURHOOD neighbourhood;    // Used to store information about neighbourhood

public:
    /* Constructors */
    NEI_READER() = default;

    /* Destructors */
    virtual ~NEI_READER() = default;

    /* Prototypes */
    void readNEIFile(const std::string &path);
    void parseNEILine();

    /* GETTERS */
    const NEIGHBOURHOOD &getNeighbourhood() const {
        return neighbourhood;
    }
};

#endif