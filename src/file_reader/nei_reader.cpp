// Simulating Site Specific Interactions_SISSI
// Copyright (C) 2019 Lukas Huber, Tanja Gesell

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "../config.hpp"    // Used for DEBUG flag
#include "nei_reader.hpp"   // Used to provide NEI_READER class

void NEI_READER::readNEIFile(const std::string &path) {
    using namespace std;
    
    ifstream nei_file(path);

    try {
        /* Check if neighbourhood file can be read */
        if (!nei_file.good()) {
            cout << "Neighbourhood file could not be opened." << endl;

            if (!DEBUG) {                               // I don't want to abort the program in DEBUG mode
                cout << "Aborting program." << endl;
                exit(EXIT_FAILURE);
            }
        }

        /* Get rid of empty lines and read other lines as long as end of file is not reached */
        while(!nei_file.eof()) {
            getline(nei_file, line);
            /* Check if line is empty and break the loop to get to next line */
            if (line.length() == 0 || line[0] == '\n' || line[0] == ' '){
                break;
            }

            parseNEILine();   

            counter++;      // Count lines   
        };
    }
    catch(exception &e) {
        cerr << e.what() << endl;
        exit(EXIT_FAILURE);
    }
    catch (...) {
        cerr << "Errors occurred while parsing: " << path << endl;
        exit(1);
    }
}

void NEI_READER::parseNEILine() {
    using namespace std;

    size_t pos = 0;
    string token;
    string token2;
    string delimiter = "|";
    string delimiter2 = ":";

    /* Parse the position */
    pos = line.find(delimiter);
    token = line.substr(0, pos);
    token.erase(0,3);
    istringstream stream(token);
    stream >> position;
    line.erase(0, pos + delimiter.length());

    /* Check wheter an error occured regarding length of nei file */
    if (position != counter) {
        cout << "counter: " << counter << '\t' << "position: " << position << endl;
        string errorMessage = "Parsing Error caused by unequal index in nei file";
        throw runtime_error(errorMessage);
    }

    /* Parse the connections to each position */
    while ((pos = line.find(delimiter2)) != string::npos) {
        token2 = line.substr(0, pos);
        istringstream stream(token2);
        stream >> connecting_position;
        line.erase(0, pos + delimiter2.length() + 1);
        line.pop_back();
        istringstream stream2(line);
        stream2 >> number;

        if (!neighbourhood.existElement(position))
            neighbourhood.addNeighbour(position, connecting_position);
    }

    /* Determine if sequence and neighbourhood have the same length */
    if (counter+1 > neighbourhood.getParsedSites()){
        neighbourhood.setParsedSites(counter+1);
    }
}