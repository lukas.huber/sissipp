// Simulating Site Specific Interactions_SISSI
// Copyright (C) 2019 Lukas Huber, Tanja Gesell

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "fasta_reader.hpp"

void FASTA_READER::parseFastaFile(string &ipath, bpp::VectorSiteContainer *sites, bpp::Alphabet *alphabet) {
    file_format = ipath.substr(ipath.find_last_of('.') + 1);

    try {
        /* Check if file format is matching to .fasta or .fa, then parse it */
        if (file_format == "fasta" || file_format == "fa") {
            bpp::Fasta fasta;       // Class to provide the framework to read in .fasta files
            bpp::VectorSequenceContainer *sequenceContainer = fasta.readSequences(ipath, alphabet);

            if (sequenceContainer->getNumberOfSequences() > 0 ){
                for (size_t i=0; i<sequenceContainer->getNumberOfSequences(); i++){
                    sites->addSequence(sequenceContainer->getSequence(i), true);
                }
            }
            else {
                cout << "Sequence file contained no sequences!" << endl;
                if (!DEBUG) {
                    cout << "Aborting program" << endl;
                    exit(EXIT_FAILURE);
                }
            }
        }
        else {
            cout << "File format of sequence file is not supported." << endl;

            if (!DEBUG) {
                cout << "Aborting program" << endl;
                exit(EXIT_FAILURE);
            }
        }
    } catch (const bpp::BadCharException &) {
        cerr << "Alphabet does not match with provided sequence." << endl;
        exit(EXIT_FAILURE);
    } catch (bpp::Exception & e) {
        cerr << "ERROR! " << e.what() << endl;
        exit(EXIT_FAILURE);
    } catch(exception& e) {
        cerr << e.what() << endl;
        exit(EXIT_FAILURE);
    } catch (...) {
        cerr << "errors occured while sequence input" << endl;
        exit(EXIT_FAILURE);
    }
}