// Simulating Site Specific Interactions_SISSI
// Copyright (C) 2019 Lukas Huber, Tanja Gesell

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SISSIPP_FASTA_READER_HPP
#define SISSIPP_FASTA_READER_HPP

#include <iostream>
#include <boost/program_options.hpp>    // Used to create variable map for to store arguments

#include "../bio++_libraries.hpp"          
#include "../config.hpp"

using namespace std;
namespace po = boost::program_options;

class FASTA_READER {
private:
    string path;
    string file_format;

public:
    /* Constructors */
    FASTA_READER() {}

    /* Destructors */
    virtual ~FASTA_READER() = default;

    void parseFastaFile(string &ipath, bpp::VectorSiteContainer *sites, bpp::Alphabet *alphabet);
};

#endif