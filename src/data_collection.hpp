// Simulating Site Specific Interactions_SISSI
// Copyright (C) 2019 Lukas Huber, Tanja Gesell

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SISSIPP_DATA_COLLECTION_HPP
#define SISSIPP_DATA_COLLECTION_HPP

#include <iostream>     // Used for visualization on command line

#include "argument_parser.hpp"
#include "bio++_libraries.hpp"
#include "file_reader/nei_reader.hpp"
#include "model.hpp"

#include "rng/rng.hpp"
#include "rng/mt19937_64.hpp"
#include "rng/jkiss_64.hpp"

#include <boost/program_options.hpp>    // Used to create variable map for to store arguments

using namespace std;

class DATA_COLLECTION {
private:
    po::variables_map vm;                   // Container for all given arguments
    NEIGHBOURHOOD neighbourhood;            // Container for neighbourhood information
    MODEL model;                            // Container for model information
    bpp::Tree *tree;                        // Container for tree information
    RNG *rng = (RNG *)malloc(sizeof(RNG));  // Container for rng information

    bpp::Alphabet *alphabet;            // Used to store sequence in suitable container
    bpp::VectorSiteContainer *sites;    // Container for site information
    bpp::Sequence *ancestral_sequence;  // Used to store the ancestral sequence, which is used for simulation

    string substitution_model;          // Used to store the user-chosen substitution model
    string rng_method;                  // Used to store the user-chosen RNG method
    
public:
    /* Constructors */
    DATA_COLLECTION(ARGUMENTS &input_arguments)  {
        vm = input_arguments.getVm();
    }

    /* Destructors */
    virtual ~DATA_COLLECTION() = default;

    /* Prototypes */
    void initNeighbourhood();
    void initTree();
    void initRNG();
    void initSequence();
    void initFrequencies();
    void initAlphabet();
    void initParameters();
    bpp::Sequence *createRandomAncestralSequence();

    /* GETTERS */
    const vector<double> &getFrequenciesFromArguments(const string &keyvalue, const unsigned int &size) const {
        if (vm.count(keyvalue)){
            if (!vm[keyvalue].empty() && (vm[keyvalue].as< vector<double> >()).size() == size) {
                return vm[keyvalue].as< vector<double> >();
            } 
            else {
                throw runtime_error("The amount of given " + keyvalue + " nucleotide frequency options is not equal to " + to_string(size));
            }
        }
        else {
            throw runtime_error("The searched keyvalue \"" + keyvalue + "\" does not exist. Error in function getFrequencies().");
        }
    }

    const string &getRNGMethod() const {
        return rng_method;
    }

    bool getSeedChoice() const {
        if (vm.count("seed"))         // Returns true if a custom seed is provided. False if a random seed should be generated
            return true;
        else
            return false;
    }

    bpp::VectorSiteContainer getAlphabetChoice() const {
        return alphabet;
    }

    /* SETTERS */
    void setSubModel() {
        if (vm.count("DS078") && vm["DS078"].as<bool>() == true)
        substitution_model = "DS078";
        else if (vm.count("JTT92") && vm["JTT92"].as<bool>() == true)
            substitution_model = "JTT92";
        else if (vm.count("WAG01") && vm["WAG01"].as<bool>() == true)
            substitution_model = "WAG01";

        else if (vm.count("GY94") && vm["GY94"].as<bool>() == true)
            substitution_model = "GY94";
        else if (vm.count("MG94") && vm["MG94"].as<bool>() == true)
            substitution_model = "MG94";
        else if (vm.count("YN98") && vm["YN98"].as<bool>() == true)
            substitution_model = "YN98";

        else if (vm.count("K80") && vm["K80"].as<bool>() == true)
            substitution_model = "K80";
        else if (vm.count("F81") && vm["F81"].as<bool>() == true)
            substitution_model = "F81";
        else if (vm.count("HKY85") && vm["HKY85"].as<bool>() == true)
            substitution_model = "HKY85";
        else if (vm.count("T92") && vm["T92"].as<bool>() == true)
            substitution_model = "T92";
        else if (vm.count("TN93") && vm["TN93"].as<bool>() == true)
            substitution_model = "TN93";
        else if (vm.count("GTR") && vm["GTR"].as<bool>() == true)
            substitution_model = "GTR";
        else if (vm.count("RN95") && vm["RN95"].as<bool>() == true)
            substitution_model = "RN95";
        else if (vm.count("SSR") && vm["SSR"].as<bool>() == true)
            substitution_model = "SSR";
        // Must be check last, as this is the DEFAULT
        else if (vm.count("JC69") && vm["JC69"].as<bool>() == true)
            substitution_model = "JC69";

        else
            throw std::runtime_error("Errors occoured while parsing model state");

        model.setName(substitution_model);
    }

    void setRNGMethod() {
        /* Set rng method and save it. Default = MT19937 */
        if (vm.count("JKISS") && vm["JKISS"].as<bool>() == true)
            rng_method = "JKISS";
        else if (vm.count("MT19937") && vm["MT19937"].as<bool>() == true)
            rng_method = "MT19937";
        else
            throw std::runtime_error("Errors occoured while setting random state");
    }
};

#endif