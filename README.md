# Simulating Site Specific Interactions (SISSI++)

**Author:** Lukas Huber, Tanja Gesell  
**Version:** 2.0  (Under development)  
**Programming Language:** C++  
**Supported operating systems:** Linux, macOS

## Description

SISSI++ (Simulating Site Specific Interactions) is a tool to simulate the site specific interactions along a phylogenetic tree based on patterns in alignments (RNA, DNA, codons, aminoacids).
Additionally the tools takes the neighbourhood into account.

*This tool is currently under development. It is not yet fit for purpose and should be used with caution.*

## Installation

### Installation Requirements

Installation is done using the terminal of your linux terminal. The packages `wget` and `make` are necessary to install SISSI++.
The following commands are only applicable for linux distributions which utilize `apt` as package manager (i.e. Ubuntu). Adapt these commands to your package manager.

```bash
sudo apt-get update
sudo apt-get install wget build-essential libboost-dev libboost-program-options-dev libbpp-core-dev libbpp-seq-dev libbpp-phyl-dev
```

### Installation of SISSI++

Navigate to the folder in which SISSI++ should be installed.
Execute the following commands inside of your terminal in your desired directory.

```bash
wget https://gitlab.com/lukas.huber/sissipp/-/archive/master/sissi-master.tar.gz
tar -xzvf sissi-master.tar.gz
rm -f sissi-master.tar.gz
make
```

## Usage

SISSI++ can be executed by navigating to the installation directory and executing it with the desired parameters (listed below)

```bash
./sissi++ -i <path> -o <path> <Additional desired Parameters>
```

### Parameters

The following table only represents the most important available parameters.
A table of all available parameters would congest this README too much.
For a complete list please refer to the help message of SISSI++.

| Parameter                     | Description                                                                   |
| ---                           |:---                                                                           |
| -h [ --help ]                 | Print help message                                                            |
| -v [ --version ]              | Print current version                                                         |
| -c [ --config ] <path>        | Set filepath to config file                                                   |
| -i [ --input ] <path> **\***  | Set filepath to input sequence file                                           |
| -a [ --alphabet ] <arg>       | Set alphabet code for sequence [RNA (=Default), DNA, PROTEIN, BINARY, CODON]  |
| -o [ --output] <path> **\***  | Set filepath to output file                                                   |
| -t [ --tree] <path>           | Set filepath to tree file                                                     |
| -n [ --neighbourhood] <path>  | Set filepath to neighbourhood file                                            |

*\* mandatory parameter* 

## Dependecies

Dependencies are already included into this repository (statically linked) and are not needed to be installed separately. They are just listed here for completeness.

* **Argument parser:** [Boost Library](https://www.boost.org/)
* **Thermodynamic data:** [Vienna RNA Package](https://www.tbi.univie.ac.at/RNA/)
* **Random number generator:** [Boost Library](https://www.boost.org/)
