// Simulating Site Specific Interactions_SISSI
// Copyright (C) 2019 Lukas Huber, Tanja Gesell

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/* STD libraries */
#include <iostream>                     // Used for visualization on terminal
#include <chrono>                       // Used for benchmarking the program (time)

/* Internal libraries */
#include "src/config.hpp"               // Used to set flags
#include "src/argument_parser.cpp"      // Used for argument parser
#include "src/data_collection.cpp"      // Used as data base
#include "src/factory.cpp"              // Heart of this software. Used for Simulation

#include "src/model.cpp"                // Ugly solution. Currently the compiler somehow doesn't link model.hpp with model.cpp.
                                        // So I had to tell the compiler, that model.cpp is in fact there. Only temporary solution!

void printLicense() {
    using std::cout, std::endl;

    cout << "Simulating Site Specific Interactions_SISSI  Copyright (C) 2019 Lukas Huber, Tanja Gesell"   << endl
         << "This program comes with ABSOLUTELY NO WARRANTY; for details see LICENSE."                    << endl
         << "This is free software, and you are welcome to redistribute it"                               << endl
         << "under certain conditions; see LICENSE for details."                                          << endl
         << endl;
}

int main (int argc, char* argv[]) {
    using std::cout, std::endl;

    auto entry_main = std::chrono::steady_clock::now(); // Remember time when program was started

    /* Print license only if program is not in DEBUG mode */
    if (!DEBUG)
        printLicense();
    else 
        cout  << "DEBUG MODE ENABLED!" << endl << endl;

    /* Get all given Arguments at command line and check them */
    ARGUMENTS argue_container;                      // Initialize container with all parsed arguments
    parseArguments(argc, argv, argue_container);    // Parses arguments from command line and store in container
    argue_container.checkRequired();                // Make sure required arguments were given

    /* Read all files and store them in data collection */
    DATA_COLLECTION database(argue_container);
    database.initRNG();                      // Initialize random number generator (and set seed if provided)
    database.setSubModel();                  // Save chosen substitution model in variable for later use
    database.initAlphabet();                 // Initialize alphabet (get choice from arguments)
    database.initNeighbourhood();            // Initialize neighbourhood (read and parse file)
    database.initTree();                     // Initialize tree (read and parse file)
    database.initSequence();                 // Initialize sequence (read and parse file)
    database.initFrequencies();              // Initialize frequencies (read and parse file or argument)
    database.initParameters();               // Initialize model parameters (read parameters from arguments)                                    

    /* Benchmarking of software */
    auto exit_main  = std::chrono::steady_clock::now();                                                  // Remember time when program was ended
    auto elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(exit_main - entry_main);     // Get total run time in ms
    auto elapsed_s  = std::chrono::duration_cast<std::chrono::seconds>(exit_main - entry_main);          // Get total run time in ms

    /* Print run time if program is in DEBUG mode */
    if (DEBUG) {
        cout << "##### BENCHMARKING DEBUG OUTPUT ######" << endl
             << "  Total time needed:\t\t" << elapsed_ms.count() << "ms" << endl  
             << "  Total time needed:\t\t"<< elapsed_s.count() << "s" << endl;
    }

    return EXIT_SUCCESS;
}